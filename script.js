/*
	Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
*/
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())

// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
.then(json => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);
});

//  == OR ==
// .then(json => {console.log(json.map(todo => (todo.title)))})

/* My answer
.then(response => {
    let titles = response.map(function(item){
        return [item.title].toString()
        })   
 		console.log(titles)
});*/


/*
	Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
*/
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then(response => console.log(response));

// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then(response => console.log(`The item "${response.title}" on the list has a status of ${response.completed}`));


/*
	Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
*/
fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			completed: false,
			id: 201,
			title: "Created To Do List Item",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));


/*
	Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
*/
fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "Updated To Do List Item",
			description: "To update the my to do list with a different data structure",
			status: "Pending",
			dateCompleted: "Pending",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));


/*
	 Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
*/
fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		// Update a to do list item by changing the status to complete and add a date when the status was changed.
		body: JSON.stringify({
			status: "Complete",
			dateCompleted: "07/09/21"
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));


/*
	Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
*/
fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "DELETE"
});
